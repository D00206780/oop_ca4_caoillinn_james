package BusinessObjects;

import Core.SharedDetails;
import DTOs.Movie;
import DTOs.User;
import Exceptions.DAOException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.*;
import java.util.*;

public class MainApp {

    //Client ID
    private int userID;

    //Pagination
    private int pageStart = 0;
    private int pageCount = -1;
    private int resultCount = 0;
    private int currentPage = 0;
    private final int RESULTS_PER_PAGE = 10;

    //JSON
    private JSONArray moviesJSONArray = new JSONArray();

    //Main Menu Enums
    enum MainMenuOptions {
        EXIT,
        REGISTER_USER,
        LOGIN,
        BROWSE_MOVIES,
        SEARCH_MOVIES,
        ADD_MOVIE,
        UPDATE_MOVIE,
        REMOVE_MOVIE,
        WATCH_MOVIE,
        RECOMMEND_MOVIE,
    }

    //Search Menu Enums
    enum SearchMenuOptions {
        RETURN_TO_MAIN_MENU,
        SEARCH_BY_TILE,
        SEARCH_BY_GENRE,
        SEARCH_BY_DIRECTOR,
        SEARCH_BY_ACTOR,
    }

    public static void main(String[] args) {
        MainApp mainApp = new MainApp();
        mainApp.startApp();
    }

    private void startApp() {
        try (Socket dataSocket = new Socket("localhost", SharedDetails.SERVER_PORT)) {
            communicateWithServer(dataSocket);
            System.out.println("\nThank you for using our service!");
            Thread.sleep(1000);
        } catch (DAOException | IOException | InterruptedException dao) {
            dao.printStackTrace();
        }
    }

    private void communicateWithServer(Socket dataSocket) throws IOException, DAOException {
        OutputStream outputToSocket = dataSocket.getOutputStream();
        PrintWriter writeOutput = new PrintWriter(new OutputStreamWriter(outputToSocket));

        InputStream inputFromSocket = dataSocket.getInputStream();
        Scanner readInput = new Scanner(new InputStreamReader(inputFromSocket));

        String message = SharedDetails.RESULT_COUNT + SharedDetails.BREAKING_CHARACTER;

        boolean continueRunning = true;
        while (continueRunning) {

            if (this.pageCount >= 0) {
                assert message != null;
                switch (message.split(SharedDetails.BREAKING_CHARACTER)[0]) {
                    case SharedDetails.END_SESSION:
                        continueRunning = false;
                        break;
                    case SharedDetails.BROWSE_MOVIES:
                        message = browseMovies();
                        break;
                    default:
                        message = openMainMenu();
                        break;
                }
            }

            if (message == null) {
                communicateWithServer(dataSocket);
            }

            writeOutput.println(message);
            writeOutput.flush();

            handleResponse(readInput.nextLine());
        }
    }

    private void handleResponse(String response) {
        String[] responseComponents = response.split(SharedDetails.BREAKING_CHARACTER);

        String responseCommand = "";
        String responseMessage = "";

        if (responseComponents.length > 0) responseCommand = responseComponents[0];
        if (responseComponents.length > 1) responseMessage = responseComponents[1];

        switch (responseCommand) {
            case SharedDetails.RESULT_COUNT:
                this.resultCount = getResultCount(responseMessage);
                this.pageCount = getPageCount();
                break;
            case SharedDetails.HELLO:
                System.out.println("\nHello, subject " + responseMessage + "!");
                storeUserId(responseMessage);
                break;
            case SharedDetails.ADD_SUCCESS:
                System.out.println("\nMovie \'" + responseMessage + "\' successfully added to the database!");
                break;
            case SharedDetails.UPDATE_SUCCESS:
                System.out.println("\nMovie \'" + responseMessage + "\' successfully updated!");
                break;
            case SharedDetails.REMOVE_SUCCESS:
                System.out.println("\nMovie \'" + responseMessage + "\' successfully removed from the database!");
                break;
            case SharedDetails.PLAY_MOVIE:
                System.out.println("\nNow watching: \'" + responseMessage + "\'.");
                break;
            case SharedDetails.DISPLAY_MOVIES:
                displayMovies(responseMessage);
                break;
            case SharedDetails.ERROR:
                System.out.println("\nError: " + responseMessage);
                break;
            case SharedDetails.UNRECOGNISED:
                System.out.println(SharedDetails.UNRECOGNISED);
                break;
        }
    }

    private String openMainMenu() throws DAOException {
        final int MIN = 0, MAX = 8;
        int userInput = Utilities.promptUserForInput(printMainMenu(), MIN, MAX);
        MainMenuOptions menuOption = MainMenuOptions.values()[userInput];

        switch (menuOption) {
            case REGISTER_USER:
                return registerUser();
            case LOGIN:
                return login();
            case BROWSE_MOVIES:
                return browseMovies();
            case SEARCH_MOVIES:
                return openSearchMoviesMenu();
            case ADD_MOVIE:
                return addMovie();
            case UPDATE_MOVIE:
                return updateMovie();
            case REMOVE_MOVIE:
                return removeMovie();
            case WATCH_MOVIE:
                return watchMovie();
            case RECOMMEND_MOVIE:
                return recommendMovie();
            case EXIT:
                return endSession();
        }
        return null;
    }

    private String printMainMenu() {
        return " \n : Please choose an option" +
                "\n1: Register" +
                "\n2: Login" +
                "\n3: View Movies" +
                "\n4: Search Movies" +
                "\n5: Add Movie" +
                "\n6: Update Movie" +
                "\n7: Remove Movie" +
                "\n8: Watch Movie" +
                "\n9: Recommend Movie" +
                "\n0: Exit" +
                "\n : ";
    }

    private String registerUser() {
        User user = new User();

        System.out.print("\nHello! Welcome to our application. Please enter a username to get started: ");
        String promptForUsername = "\nPlease enter a username: ";
        user.setUsername(Utilities.promptUserForUsername(promptForUsername));

        System.out.print("\nPerfect! Now set up your password: ");
        String promptForPassword = "\nPlease enter a password: ";
        user.setPassword(Utilities.promptUserForPassword(promptForPassword));

        System.out.print("\nWhat is your first name? ");
        String promptForFirstName = "\nPlease enter your first name: ";
        user.setFirstName(Utilities.promptUserForName(promptForFirstName));

        System.out.print("\nWhat is your last name? ");
        String promptForLastName = "\nPlease enter your last name: ";
        user.setLastName(Utilities.promptUserForName(promptForLastName));

        JSONObject userJSONObject = new JSONObject(user.toJSON());
        return SharedDetails.REGISTER + SharedDetails.BREAKING_CHARACTER + userJSONObject.toString();
    }

    private String login() {
        System.out.println("\nWelcome back!");

        String promptForUsername = "\nPlease enter your username: ";
        System.out.print(promptForUsername);
        String username = Utilities.promptUserForUsername(promptForUsername);

        String promptForPassword = "\nPlease enter your password: ";
        System.out.print(promptForPassword);
        String password = Utilities.promptUserForPassword(promptForPassword);

        return SharedDetails.LOGIN + SharedDetails.BREAKING_CHARACTER + "{" + "\"username\":" + "\"" + username + "\"" + "," + "\"password\":" + "\"" + password + "\"" + "}";
    }

    private String browseMovies() {
        if (!navigateMovies()) return null;
        return SharedDetails.BROWSE_MOVIES + SharedDetails.BREAKING_CHARACTER + "{" + "\"pageStart\":" + "\"" + this.pageStart + "\"" + "," + "\"resultsPerPage\":" + "\"" + this.RESULTS_PER_PAGE + "\"" + "}";
    }

    private String openSearchMoviesMenu() throws DAOException {

        final int MIN = 0, MAX = 4;
        int userInput = Utilities.promptUserForInput(printSearchMoviesMenu(), MIN, MAX);
        SearchMenuOptions menuOption = SearchMenuOptions.values()[userInput];

        switch (menuOption) {
            case SEARCH_BY_TILE:
                return searchMoviesByTitle();
            case SEARCH_BY_GENRE:
                return searchMoviesByGenre();
            case SEARCH_BY_DIRECTOR:
                return searchMoviesByDirector();
            case SEARCH_BY_ACTOR:
                return searchMoviesByActor();
            case RETURN_TO_MAIN_MENU:
                return openMainMenu();
        }
        return null;
    }

    private String printSearchMoviesMenu() {
        return "\n : How would you like to search?" +
                "\n1: Search by Title" +
                "\n2: Search by Genre" +
                "\n3: Search by Director" +
                "\n4: Search by Actor" +
                "\n0: Main Menu " +
                "\n : ";
    }

    private String searchMoviesByTitle() {
        System.out.println("\nSearching by title");
        String promptForTitle = "\nPlease enter a title: ";
        String title = Utilities.promptUserForString(promptForTitle);
        return SharedDetails.SEARCH_MOVIE_BY_TITLE + SharedDetails.BREAKING_CHARACTER + title;
    }

    private String searchMoviesByGenre() {
        System.out.println("\nSearching by genre");
        String promptForGenre = "\nPlease enter a genre: ";
        String genre = Utilities.promptUserForString(promptForGenre);
        return SharedDetails.SEARCH_MOVIE_BY_GENRE + SharedDetails.BREAKING_CHARACTER + genre;
    }

    private String searchMoviesByDirector() {
        System.out.println("\nSearching by director");
        String promptForDirector = "\nPlease enter a directors name: ";
        String director = Utilities.promptUserForString(promptForDirector);
        return SharedDetails.SEARCH_MOVIE_BY_DIRECTOR + SharedDetails.BREAKING_CHARACTER + director;
    }

    private String searchMoviesByActor() {
        System.out.println("\nSearching by actor");
        String promptForActor = "\nPlease enter an actors name: ";
        String actor = Utilities.promptUserForString(promptForActor);
        return SharedDetails.SEARCH_MOVIE_BY_ACTOR + SharedDetails.BREAKING_CHARACTER + actor;
    }

    private String addMovie() {
        int MIN = 1888, MAX = Calendar.getInstance().get(Calendar.YEAR);

        System.out.println("\nAdding movie");

        String promptForTitle = "\nWhat is the name of the movie that you would like to add? ";
        String title = Utilities.promptUserForString(promptForTitle);

        String promptForGenre = "\nWhat genre does this movie belong to? ";
        String genre = Utilities.promptUserForString(promptForGenre);

        String promptForDirector = "\nWho directed this movie? ";
        String director = Utilities.promptUserForString(promptForDirector);

        String promptForYear = "\nWhat year was this movie released? ";
        String year = Integer.toString(Utilities.promptUserForInput(promptForYear, MIN, MAX));

        JSONObject jsonObject = new JSONObject(new Movie(title, genre, director, year).toJSON());
        return SharedDetails.ADD_MOVIE + SharedDetails.BREAKING_CHARACTER + jsonObject.toString();
    }

    private String updateMovie() {
        System.out.println("\nUpdating movie");

        int userInput = getMovieIdFromUser();
        if (userInput == 0) return browseMovies();

        String promptForTitle = "\nWhat would you like to change the title to? ";
        String title = Utilities.promptUserForString(promptForTitle);

        String promptForRating = "\nWhat would you like to change the rating to? ";
        String rating = Utilities.promptUserForString(promptForRating);

        String promptForUserRating = "\nWhat would you like to change the user rating to? ";
        String userRating = Utilities.promptUserForString(promptForUserRating);

        String promptForFormat = "\nWhat would you like to change the format to? ";
        String format = Utilities.promptUserForString(promptForFormat);

        JSONObject jsonObject = new JSONObject(new Movie(userInput, title, rating, userRating, format).toJSON());
        return SharedDetails.UPDATE_MOVIE + SharedDetails.BREAKING_CHARACTER + jsonObject.toString();
    }

    private String removeMovie() {
        System.out.println("\nRemove movie");
        int userInput = getMovieIdFromUser();
        if (userInput == 0) return browseMovies();
        return SharedDetails.REMOVE_MOVIE + SharedDetails.BREAKING_CHARACTER + userInput;
    }

    private String watchMovie() {
        System.out.println("\nWatch movie");
        int userInput = getMovieIdFromUser();
        if (userInput == 0) return browseMovies();
        return SharedDetails.WATCH_MOVIE + SharedDetails.BREAKING_CHARACTER + userInput;
    }

    private int getMovieIdFromUser() {
        final int MIN = 0, MAX = 5000;
        System.out.println("\nPlease enter a movies ID. Press 0 if you would like to view movies first.");
        String prompt = "\nPlease enter a movie ID: ";
        return Utilities.promptUserForInput(prompt, MIN, MAX);
    }

    private String recommendMovie() {
        System.out.println("\nRecommend Movie");
        return SharedDetails.RECOMMEND_MOVIE + SharedDetails.BREAKING_CHARACTER + userID;
    }

    private String endSession() {
        return SharedDetails.END_SESSION;
    }

    private void storeUserId(String userId) {
        this.userID = Integer.parseInt(userId);
    }

    private void displayMovies(String responseJSONMessage) {
        JSONObject moviesJSONObject = new JSONObject();
        this.moviesJSONArray = new JSONArray(responseJSONMessage);

        System.out.printf("\n| %6s | ", "Movies");
        System.out.printf("\n| %6s | %50s | %50s | %50s | %10s | %10s |\n", "ID", "Title", "Genre", "Director", "Year", "Rating");

        for (int i = 0; i < this.moviesJSONArray.length(); i++) {
            moviesJSONObject.put("movie", this.moviesJSONArray.get(i));
            System.out.printf(
                    "| %6s | %50s | %50s | %50s | %10s | %10s |\n",
                    moviesJSONObject.getJSONObject("movie").get("id"),
                    moviesJSONObject.getJSONObject("movie").get("title"),
                    moviesJSONObject.getJSONObject("movie").get("genre"),
                    moviesJSONObject.getJSONObject("movie").get("director"),
                    moviesJSONObject.getJSONObject("movie").get("year"),
                    moviesJSONObject.getJSONObject("movie").get("rating")
            );
        }
    }

    private boolean navigateMovies() {
        int pageEnd = pageStart + this.RESULTS_PER_PAGE;
        boolean continueNavigating = true;

        if (this.moviesJSONArray.length() > 0) {
            int MIN = 0, MAX = 2;
            String prompt = "\n : Please select an option" +
                    "\n1: Navigate Back" +
                    "\n2: Navigate Forward" +
                    "\n0: Exit Navigation" +
                    "\n : ";

            System.out.println("\nShowing results " + this.pageStart + " - " + pageEnd + ". Page " + this.currentPage + " of " + this.pageCount);
            int userInput = Utilities.promptUserForInput(prompt, MIN, MAX);

            switch (userInput) {
                case 1:
                    navigateBackThroughMovies();
                    break;
                case 2:
                    navigateForwardThroughMovies();
                    break;
                case 0:
                    continueNavigating = false;
                    endNavigation();
                    break;
            }
        }
        return continueNavigating;
    }

    private void navigateForwardThroughMovies() {
        if (this.pageStart + this.RESULTS_PER_PAGE >= this.resultCount) {
            this.pageStart = this.resultCount - this.RESULTS_PER_PAGE;
        } else {
            this.pageStart += this.RESULTS_PER_PAGE;
            this.currentPage++;
        }
    }

    private void navigateBackThroughMovies() {
        if (this.pageStart - this.RESULTS_PER_PAGE <= 0) {
            this.pageStart = 0;
        } else {
            this.pageStart -= this.RESULTS_PER_PAGE;
            this.currentPage--;
        }
    }

    private void endNavigation() {
        this.pageStart = 0;
        this.moviesJSONArray = new JSONArray();
    }

    private int getResultCount(String results) {
        return Integer.parseInt(results);
    }

    private int getPageCount() {
        return (this.resultCount - (this.resultCount % this.RESULTS_PER_PAGE)) / this.RESULTS_PER_PAGE;
    }
}