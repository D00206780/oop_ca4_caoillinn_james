package BusinessObjects;

import java.util.Scanner;

/**
 * Class snippet abstracted from previous project
 * @author James Farrell
 */
class Utilities {

    private static final Scanner TXT_SC = new Scanner(System.in);
    private static final Scanner NUM_SC = new Scanner(System.in);

    /**
     * Prompts the user to enter an input.
     *
     * @param prompt prompt user for input
     * @param min    minimum input value
     * @param max    maximum input value
     * @return input value if acceptable
     */
    static int promptUserForInput(String prompt, int min, int max) {
        while (true) {
            System.out.print(prompt);
            if (NUM_SC.hasNextInt()) {
                int input = NUM_SC.nextInt();
                if (input < min || input > max) {
                    System.out.println("\nInvalid entry. Please enter a number "
                            + "between " + min + " and " + max + ".");
                } else {
                    return input;
                }
            } else {
                System.out.println("\nInvalid entry. Please enter a valid number.");
                NUM_SC.next();
            }
        }
    }

    /**
     * Prompts the user to enter a name.
     *
     * @param prompt prompt user for input
     * @return input value if acceptable
     */
    static String promptUserForName(String prompt) {
        while (true) {
            String name = TXT_SC.nextLine();
            if (name.length() < 2 || name.length() > 36) {
                System.out.println("\nPlease enter a name between 2 and 36 letters long.");
            } else if (scanForBreakingCharacter(name)) {
                System.out.println("\nPlease don't break me!");
            } else {
                return name;
            }
            System.out.print(prompt);
        }
    }

    /**
     * Prompts the user to enter a string.
     *
     * @param prompt user for input
     * @return input value if acceptable
     */
    static String promptUserForString(String prompt) {
        while (true) {
            System.out.print(prompt);
            String string = TXT_SC.nextLine();
            if (string.length() < 1 || string.length() > 36) {
                System.out.println("\nPlease enter a string between 1 and 36 letters long.");
            } else if (scanForBreakingCharacter(string)) {
                System.out.println("\nPlease don't break me!");
            } else {
                return string;
            }
        }
    }

    /**
     * Prompts the user to enter a username.
     *
     * @param prompt user for input
     * @return input value if acceptable
     */
    static String promptUserForUsername(String prompt) {
        while (true) {
            String username = TXT_SC.nextLine();
            if (username.length() < 8 || username.length() > 16) {
                System.out.println("\nPlease enter a username between 8 and 16 letters long.");
            } else if (!username.matches("^[A-Za-z0-9]+$")) {
                System.out.println("\nUsernames may only contain letters and numbers");
            } else if (scanForBreakingCharacter(username)) {
                System.out.println("\nPlease don't break me!");
            } else {
                return username;
            }
            System.out.print(prompt);
        }
    }

    /**
     * Prompts the user to enter a password.
     * Regex pattern taken from:
     * https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
     *
     * @param prompt user for input
     * @return input value if acceptable
     */
    static String promptUserForPassword(String prompt) {
        while (true) {
            String password = TXT_SC.nextLine();
            if (password.length() < 8 || password.length() > 16) {
                System.out.println("\nPlease enter a password between 8 and 16 letters long.");
            } else if (!password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,16}$")) {
                System.out.println("\nPasswords must contain at least one uppercase letter, one lowercase letter, and one number.");
            } else if (scanForBreakingCharacter(password)) {
                System.out.println("\nPlease don't break me!");
            } else {
                return password;
            }
            System.out.print(prompt);
        }
    }

    /**
     * Scans a user input string for use of our breaking character
     *
     * @param input user input
     * @return true if the user has entered our breaking character
     */
    private static boolean scanForBreakingCharacter(String input) {
        int count = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '%') {
                count++;
            } else if (count > 0) {
                count--;
            }
            if (count == 2) {
                return true;
            }
        }
        return false;
    }
}