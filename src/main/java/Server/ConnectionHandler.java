package Server;

import Core.SharedDetails;
import DAOs.*;
import DTOs.Movie;
import DTOs.User;
import Exceptions.DAOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ConnectionHandler implements Runnable {

    //Create cache for storing client queries - The cache will clear after 10 minutes
    private Cache<String, String> serverCache = new Cache<>(10 * 60 * 1000);

    //Create interfaces with DAO objects
    private MovieDAOInterface IMovieDAO = new MySQLMovieDAO();
    private UserDAOInterface IUserDAO = new MySQLUserDAO();
    //Store client socket
    private Socket socketToHandle;

    //Store unserviced clients
    private static final List<Socket> pool = new LinkedList<>();

    //Process incoming requests from a client
    static void processRequest(Socket incomingClient) {

        //Lock the pool, or wait for it to unlock
        synchronized (pool) {

            //Append a client to the end of the linked list
            pool.add(pool.size(), incomingClient);

            //Notify all waiting threads that a client is now ready to be serviced
            pool.notifyAll();
        }
    }

    @Override
    public void run() {
        while (true) {

            //Lock the pool, or wait for it to unlock
            synchronized (pool) {

                //Loop while the pool is empty
                while (pool.isEmpty()) {

                    try {

                        //Wait until a client has entered the pool
                        pool.wait();
                    } catch (InterruptedException ie) {
                        return;
                    }
                }

                //Retrieve the client from the pool
                socketToHandle = pool.remove(0);

                //Handle the client
                handleConnection();
            }
        }
    }

    private void handleConnection() {

        String command = "";
        String request = "";
        String response;

        try {
            InetAddress ip = socketToHandle.getInetAddress();
            Anton.LOGGER.info("Client connected: " + ip);

            OutputStream outputToSocket = socketToHandle.getOutputStream();
            PrintWriter writeOutput = new PrintWriter(new OutputStreamWriter(outputToSocket));

            InputStream inputFromSocket = socketToHandle.getInputStream();
            Scanner readInput = new Scanner(new InputStreamReader(inputFromSocket));
            String incomingMessage;

            while (true) {
                if (readInput.hasNextLine()) {
                    incomingMessage = readInput.nextLine();

                    String[] components = incomingMessage.split(SharedDetails.BREAKING_CHARACTER);

                    if (components.length > 0) {
                        command = components[0];
                        Anton.LOGGER.info("Client " + ip + " command: " + command);
                    }

                    if (components.length > 1) {
                        request = components[1];
                        Anton.LOGGER.info("Client " + ip + " request: " + request);
                    }

                    if (serverCache.containsKey(incomingMessage)) {
                        response = serverCache.get(incomingMessage);
                    } else {
                        switch (command) {
                            case SharedDetails.RESULT_COUNT:
                                response = getResultCount();
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.REGISTER:
                                response = registerUser(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.LOGIN:
                                response = login(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.SEARCH_MOVIE_BY_TITLE:
                                response = searchMoviesByTitle(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.SEARCH_MOVIE_BY_GENRE:
                                response = searchMoviesByGenre(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.SEARCH_MOVIE_BY_DIRECTOR:
                                response = searchMoviesByDirector(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.SEARCH_MOVIE_BY_ACTOR:
                                response = searchMoviesByActor(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.ADD_MOVIE:
                                response = addMovie(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.REMOVE_MOVIE:
                                response = removeMovie(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.UPDATE_MOVIE:
                                response = updateMovie(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.BROWSE_MOVIES:
                                response = browseMovies(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.WATCH_MOVIE:
                                response = watchMovie(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.RECOMMEND_MOVIE:
                                response = recommendMovie(request);
                                serverCache.put(incomingMessage, response);
                                break;
                            case SharedDetails.END_SESSION:
                                response = endSession();
                                serverCache.put(incomingMessage, response);
                                break;
                            default:
                                response = unrecognised();
                                break;
                        }
                    }

                    writeOutput.println(response);
                    writeOutput.flush();

                } else {
                    Anton.LOGGER.info("Client " + ip + " disconnected!");
                    break;
                }
            }
        } catch (DAOException DAOe) {
            Anton.LOGGER.warning("DAOException caught: " + DAOe);
        } catch (IOException IOe) {
            Anton.LOGGER.warning("IOException caught: " + IOe);
        }
    }

    private String getResultCount() throws DAOException {
        return SharedDetails.RESULT_COUNT + SharedDetails.BREAKING_CHARACTER + IMovieDAO.getRowCount();
    }

    private synchronized String registerUser(String userJSONString) throws DAOException {
        User user = convertJSONToUser(new JSONObject(userJSONString));
        if (IUserDAO.checkIfUserExists(user))
            return SharedDetails.ERROR + SharedDetails.BREAKING_CHARACTER + SharedDetails.USER_EXISTS;
        if (!IUserDAO.registerUser(user))
            return SharedDetails.ERROR + SharedDetails.BREAKING_CHARACTER + SharedDetails.REGISTER_ERROR;
        return SharedDetails.HELLO + SharedDetails.BREAKING_CHARACTER + IUserDAO.getLastInsertId();
    }

    private String login(String userJSONString) throws DAOException {
        JSONObject userJSON = new JSONObject(userJSONString);
        String username = userJSON.getString("username");
        String password = userJSON.getString("password");
        User user = IUserDAO.findUserByUsernamePassword(username, password);
        if (user != null) return SharedDetails.HELLO + SharedDetails.BREAKING_CHARACTER + user.getId();
        return SharedDetails.ERROR + SharedDetails.BREAKING_CHARACTER + SharedDetails.LOGIN_ERROR;
    }

    private String browseMovies(String requestJSONString) throws DAOException {
        JSONObject jsonObject = new JSONObject(requestJSONString);
        int pageStart = Integer.parseInt(jsonObject.getString("pageStart"));
        int resultsPerPage = Integer.parseInt(jsonObject.getString("resultsPerPage"));
        List<Movie> movies = IMovieDAO.paginateMovies(pageStart, resultsPerPage);
        JSONArray jsonArray = new JSONArray();
        for (Movie m : movies) {
            jsonObject = new JSONObject(m);
            jsonArray.put(jsonObject);
        }
        return SharedDetails.DISPLAY_MOVIES + SharedDetails.BREAKING_CHARACTER + jsonArray.toString();
    }

    private String searchMoviesByTitle(String title) throws DAOException {
        JSONArray jsonArray = new JSONArray(IMovieDAO.findMoviesByTitle(title));
        return SharedDetails.DISPLAY_MOVIES + SharedDetails.BREAKING_CHARACTER + jsonArray.toString();
    }

    private String searchMoviesByGenre(String genre) throws DAOException {
        JSONArray jsonArray = new JSONArray(IMovieDAO.findMoviesByGenre(genre));
        return SharedDetails.DISPLAY_MOVIES + SharedDetails.BREAKING_CHARACTER + jsonArray.toString();
    }

    private String searchMoviesByDirector(String director) throws DAOException {
        JSONArray jsonArray = new JSONArray(IMovieDAO.findMoviesByDirector(director));
        return SharedDetails.DISPLAY_MOVIES + SharedDetails.BREAKING_CHARACTER + jsonArray.toString();
    }

    private String searchMoviesByActor(String actor) throws DAOException {
        JSONArray jsonArray = new JSONArray(IMovieDAO.findMoviesByActor(actor));
        return SharedDetails.DISPLAY_MOVIES + SharedDetails.BREAKING_CHARACTER + jsonArray.toString();
    }

    private synchronized String addMovie(String movieJSONString) throws DAOException {
        Movie movie = convertJSONToMovieForAdd(new JSONObject(movieJSONString));
        if (IMovieDAO.checkIfMovieExists(movie))
            return SharedDetails.ERROR + SharedDetails.BREAKING_CHARACTER + SharedDetails.MOVIE_EXISTS;
        if (!IMovieDAO.addMovie(movie))
            return SharedDetails.ERROR + SharedDetails.BREAKING_CHARACTER + SharedDetails.ADD_ERROR;
        return SharedDetails.ADD_SUCCESS + SharedDetails.BREAKING_CHARACTER + movie.getTitle();
    }

    private synchronized String updateMovie(String movieJSONString) throws DAOException {
        System.out.println(movieJSONString);
        Movie movie = convertJSONToMovieForUpdate(new JSONObject(movieJSONString));
        if (!IMovieDAO.checkIfMovieExists(movie.getId()))
            return SharedDetails.ERROR + SharedDetails.BREAKING_CHARACTER + SharedDetails.MOVIE_DOES_NOT_EXIST;
        if (!IMovieDAO.updateMovie(movie))
            return SharedDetails.ERROR + SharedDetails.BREAKING_CHARACTER + SharedDetails.UPDATE_ERROR;
        return SharedDetails.UPDATE_SUCCESS + SharedDetails.BREAKING_CHARACTER + movie.getTitle();
    }

    private synchronized String removeMovie(String movieId) throws DAOException {
        Movie movie = IMovieDAO.getMovieById(Integer.parseInt(movieId));
        if (IMovieDAO.removeMovie(Integer.parseInt(movieId)))
            return SharedDetails.REMOVE_SUCCESS + SharedDetails.BREAKING_CHARACTER + movie.getTitle();
        return SharedDetails.ERROR + SharedDetails.BREAKING_CHARACTER + SharedDetails.REMOVE_ERROR;
    }

    private String watchMovie(String movieId) throws DAOException {
        Movie movie = IMovieDAO.watchMovie(Integer.parseInt(movieId));
        return SharedDetails.PLAY_MOVIE + SharedDetails.BREAKING_CHARACTER + movie.getTitle();
    }

    private String recommendMovie(String userId) throws DAOException {
        List<String> recommendations = IMovieDAO.recommendMovies(Integer.parseInt(userId));
        JSONArray recommendationsJSON = new JSONArray();
        for (String recommendation : recommendations) {
            recommendationsJSON.put(new JSONObject(recommendation));
        }
        return SharedDetails.DISPLAY_MOVIES + SharedDetails.BREAKING_CHARACTER + recommendationsJSON.toString();
    }

    private String endSession() {
        return SharedDetails.END_SESSION;
    }

    private String unrecognised() {
        return SharedDetails.UNRECOGNISED;
    }

    private Movie convertJSONToMovieForAdd(JSONObject movieJSON) throws JSONException {
        System.out.println(movieJSON.toString());
        String title = movieJSON.getString("title");
        String genre = movieJSON.getString("genre");
        String director = movieJSON.getString("director");
        String year = movieJSON.getString("year");
        return new Movie(title, genre, director, year);
    }

    private Movie convertJSONToMovieForUpdate(JSONObject movieJSON) throws JSONException {
        System.out.println(movieJSON.toString());
        int id = movieJSON.getInt("id");
        String title = movieJSON.getString("title");
        String rating = movieJSON.getString("rating");
        String userRating = movieJSON.getString("userRating");
        String format = movieJSON.getString("format");
        return new Movie(id, title, rating, userRating, format);
    }

    private User convertJSONToUser(JSONObject userJSON) throws JSONException {
        String firstname = userJSON.getString("firstname");
        String lastname = userJSON.getString("lastname");
        String username = userJSON.getString("username");
        String password = userJSON.getString("password");
        return new User(firstname, lastname, username, password);
    }
}