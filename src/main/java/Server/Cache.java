package Server;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Cache<K, V> extends HashMap<K, V> {
    //private static final long serialVersionUID = 1l;

    private Map<K, V> timeMap = new HashMap<K, V>();
    private long expiryInMilliS;
    private boolean mapAlive = true;
    private long expiry;


    public Cache() {
        this.expiryInMilliS = 10000;
        initialize();
    }

    public Cache(long expiryInMilliS) {
        this.expiryInMilliS = expiryInMilliS;
        initialize();
    }

    /**
     * This method initializes the cache creating the cleaner thread and setting an expiry date for the cache
     */
    void initialize() {
        new Cleaner().start();
        expiry = new Date().getTime();
    }

    public long getExpiry() {
        return expiry;
    }

    /**
     * Overrides the put method from the HashMap superclass - checks if the map is alive and if it is will put a key and value into the map
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public V put(K key, V value) {
        if (!mapAlive) {
            throw new IllegalStateException("Cache is not alive... Create a new one");
        }


        timeMap.put(key, value);
        V returnValue = super.put(key, value);

        return returnValue;
    }


    /*
        This is a cleaner thread for the Cache
        The thread will start when the cache is created - due to the
        initialize() method.

        The cleaner will check the current time againts the expiry time and using the
        length of time setup by the user will clear the map - clearing the cache.
     */

    class Cleaner extends Thread {
        @Override
        public void run() {
            while (mapAlive) {
                cleanMap();
                try {
                    Thread.sleep(expiryInMilliS / 2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Clean map loops through the keys in the maps and removes them when
         * the expiry time is up
         */
        private void cleanMap() {
            long currentTime = new Date().getTime();
            for (K key : timeMap.keySet()) {
                if (currentTime > (getExpiry() + expiryInMilliS)) {
                    V value = remove(key);
                    timeMap.remove(key);
                }
            }
        }
    }

}
