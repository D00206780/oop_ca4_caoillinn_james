package Server;

public class CacheTest {
    public static void main(String[] args) {
        Cache<String, String> cache = new Cache<>(1 * 60 * 1000);

        cache.put("Hello", "World");

        if(cache.containsKey("Hello")){
            System.out.println("It's there");
        }

        if(cache.containsKey("Goodblye")){
            System.out.println("It's there");
        }else{
            System.out.println("Goodbye is not there");
        }
    }
}
