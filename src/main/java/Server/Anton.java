package Server;

import Core.SharedDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Anton {

    //Logging
    static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final Level LOG_LEVEL = Level.INFO;

    public static void main(String[] args) {
        Anton anton = new Anton();
        anton.start();
    }

    private void start() {

        //Set up server logger
        setUpLogger();

        //Set up handler threads in the pool
        setUpHandlers();

        //Accept connections
        acceptConnections();
    }

    private void setUpLogger() {
        try {
            //Set up log level
            LOGGER.setLevel(LOG_LEVEL);

            //Set up logging file
            FileHandler logFile = new FileHandler("Server.log", true);

            //Set up text formatter
            logFile.setFormatter(new SimpleFormatter());

            //Allow the logger to handle the log file
            LOGGER.addHandler(logFile);

            //Log to file
            LOGGER.info("Hello! My name is Anton..");
        } catch (IOException ioe) {
            System.out.println("Error logging to file: " + ioe.getMessage());
        }
    }

    private void setUpHandlers() {

        //Create pool of reusable threads
        for (int i = 0; i < SharedDetails.MAX_CONNECTIONS; i++) {

            //Create thread
            ConnectionHandler clientHandler = new ConnectionHandler();
            Thread clientThread = new Thread(clientHandler);

            //Start thread
            clientThread.start();
        }
    }

    private void acceptConnections() {
        try {
            //Open a listening socket
            ServerSocket listeningSocket = new ServerSocket(SharedDetails.SERVER_PORT, 5);

            //Create a thread group
            ThreadGroup group = new ThreadGroup("Client Threads");

            //Set the max priority of each thread in the group, to be below the current thread priority
            group.setMaxPriority(Thread.currentThread().getPriority() - 1);

            //Live
            while (true) {

                //Accept a new client
                Socket incomingConnection = listeningSocket.accept();

                //Handle client connection
                handleConnection(incomingConnection);
            }

            //Close the connection - only necessary where it is possible to end the above loop
            //listeningSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleConnection(Socket connectionToHandle) {

        //Process client request
        ConnectionHandler.processRequest(connectionToHandle);
    }
}