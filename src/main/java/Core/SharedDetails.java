package Core;

public class SharedDetails {

    //Config details
    public static final int SERVER_PORT = 50005;
    public static final int MAX_CONNECTIONS = 10;
    public static final String BREAKING_CHARACTER = "%%";

    //Commands to be sent to the server
    public static final String REGISTER = "RGST";
    public static final String LOGIN = "LOGN";
    public static final String SEARCH_MOVIE_BY_TITLE = "TITL";
    public static final String SEARCH_MOVIE_BY_GENRE = "GNRE";
    public static final String SEARCH_MOVIE_BY_DIRECTOR = "DRCT";
    public static final String SEARCH_MOVIE_BY_ACTOR = "ACTR";
    public static final String ADD_MOVIE = "CRTE";
    public static final String REMOVE_MOVIE = "RMVE";
    public static final String UPDATE_MOVIE = "UPDT";
    public static final String WATCH_MOVIE = "WTCH";
    public static final String RECOMMEND_MOVIE = "RCMD";
    public static final String END_SESSION = "KILL";
    public static final String BROWSE_MOVIES = "BRWS";

    //Shared Response
    public static final String RESULT_COUNT = "RCNT";

    //Server Responses
    public static final String ERROR = "ERRR";
    public static final String HELLO = "HELO";
    public static final String ADD_SUCCESS = "ASCS";
    public static final String UPDATE_SUCCESS = "USCS";
    public static final String REMOVE_SUCCESS = "RSCS";
    public static final String PLAY_MOVIE = "PLAY";
    public static final String DISPLAY_MOVIES = "DSPL";

    //Server messages
    public static final String UNRECOGNISED = "Unrecognised command. Please try again!";
    public static final String USER_EXISTS = "A user with that username already exists! Please pick a different one.";
    public static final String MOVIE_EXISTS = "We already have that movie, but thank you for trying!";
    public static final String MOVIE_DOES_NOT_EXIST = "That movie does not exist! Please try again.";
    public static final String UPDATE_ERROR = "There was an issue while updating a movie";
    public static final String REMOVE_ERROR = "There was an issue while adding a movie";
    public static final String REGISTER_ERROR = "There was an issue when registering.";
    public static final String LOGIN_ERROR = "There was an issue when logging in.";
    public static final String ADD_ERROR = "There was an issue when adding a movie.";
}