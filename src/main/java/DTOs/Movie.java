package DTOs;

public class Movie {

    private int id;
    private String title;
    private String genre;
    private String director;
    private String runtime;
    private String plot;
    private String location;
    private String poster;
    private String rating;
    private String format;
    private String year;
    private String starring;
    private String copies;
    private String barcode;
    private String userRating;

    public Movie(int id, String title, String genre, String director, String runtime, String plot, String location, String poster, String rating, String format, String year, String starring, String copies, String barcode, String userRating) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.runtime = runtime;
        this.plot = plot;
        this.location = location;
        this.poster = poster;
        this.rating = rating;
        this.format = format;
        this.year = year;
        this.starring = starring;
        this.copies = copies;
        this.barcode = barcode;
        this.userRating = userRating;
    }

    public Movie(String title, String genre, String director, String year) {
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.year = year;
    }

    public Movie(int id, String title, String rating, String userRating, String format) {
        this.id = id;
        this.title = title;
        this.rating = rating;
        this.userRating = userRating;
        this.format = format;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public String getYear() {
        return year;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getPlot() {
        return plot;
    }

    public String getLocation() {
        return location;
    }

    public String getPoster() {
        return poster;
    }

    public String getRating() {
        return rating;
    }

    public String getFormat() {
        return format;
    }

    public String getStarring() {
        return starring;
    }

    public String getCopies() {
        return copies;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getUserRating() {
        return userRating;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setYear(int year) {
        this.year = Integer.toString(year);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", director='" + director + '\'' +
                ", runtime='" + runtime + '\'' +
                ", plot='" + plot + '\'' +
                ", location='" + location + '\'' +
                ", poster='" + poster + '\'' +
                ", rating='" + rating + '\'' +
                ", format='" + format + '\'' +
                ", year='" + year + '\'' +
                ", starring='" + starring + '\'' +
                ", copies='" + copies + '\'' +
                ", barcode='" + barcode + '\'' +
                ", user_rating='" + userRating + '\'' +
                '}';
    }

    public String toJSON() {
        return "{" + "\"id\":" + "\"" + this.id + "\"," + "\"title\":" + "\"" + this.title + "\","
                + "\"genre\":" + "\"" + this.genre + "\"," + "\"director\":" + "\"" + this.director + "\","
                + "\"runtime\":" + "\"" + this.runtime + "\"," + "\"plot\":" + "\"" + this.plot + "\","
                + "\"location\":" + "\"" + this.location + "\"," + "\"poster\":" + "\"" + this.poster + "\","
                + "\"rating\":" + "\"" + this.rating + "\"," + "\"format\":" + "\"" + this.format + "\","
                + "\"year\":" + "\"" + this.year + "\"," + "\"starring\":" + "\"" + this.starring + "\","
                + "\"copies\":" + "\"" + this.copies + "\"," + "\"barcode\":" + "\"" + this.barcode + "\","
                + "\"userRating\":" + "\"" + this.userRating + "\"}";
    }
}