package DTOs;

import java.util.ArrayList;
import java.util.Arrays;

public class User {
    private int id;
    private String firstname;
    private String lastname;
    private String username;
    private String password;

    public User(String firstname, String lastname, String username, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
    }

    public User(int id, String firstname, String lastname, String username, String password) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setFirstName(String firstname) {
        this.firstname = firstname;
    }

    public void setLastName(String lastname) {
        this.lastname = lastname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstname + '\'' +
                ", lastName='" + lastname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String toJSON() {
        return "{" + "\"id\":" + "\"" + this.id + "\"," + "\"firstname\":" + "\"" + this.firstname + "\","
                + "\"lastname\":" + "\"" + this.lastname + "\"," + "\"username\":" + "\"" + this.username + "\","
                + "\"password\":" + "\"" + this.password + "\"}";
    }

    //https://stackoverflow.com/questions/10559996/array-list-inside-constructor-as-one-of-the-parameter-how-to-create-a-new-objec
    public ArrayList<String> toArrayList() {
        return new ArrayList<>(Arrays.asList(firstname, lastname, username, password));
    }
}