package DAOs;

import DTOs.Movie;
import Exceptions.DAOException;

import java.util.List;

public interface MovieDAOInterface {

    //Get row count
    int getRowCount() throws DAOException;

    //Get movie
    Movie getMovieById(int id) throws DAOException;

    //Display movies
    List<Movie> paginateMovies(int pageStart, int resultsPerPage) throws DAOException;

    //Search Movies
    List<Movie> findMoviesByTitle(String title) throws Exceptions.DAOException;
    List<Movie> findMoviesByDirector(String director) throws DAOException;
    List<Movie> findMoviesByGenre(String genre) throws DAOException;
    List<Movie> findMoviesByActor(String actor) throws DAOException;

    //Add movie
    boolean addMovie(Movie movie) throws DAOException;

    //Update movie
    boolean updateMovie(Movie movie) throws DAOException;

    //Remove movie
    boolean removeMovie(int movieId) throws DAOException;

    //Watch movie
    Movie watchMovie(int movieId) throws DAOException;

    //Recommend Movie
    List<String> recommendMovies(int userId) throws DAOException;

    //Check if movie exists
    boolean checkIfMovieExists(Movie movie) throws DAOException;
    boolean checkIfMovieExists(int movieId) throws DAOException;
}