package DAOs;

import DTOs.User;
import Exceptions.DAOException;

public interface UserDAOInterface {

    //Find user
    User findUserByUsernamePassword(String username, String password) throws DAOException;

    //Get last stored id
    int getLastInsertId() throws DAOException;

    //Check if user exists
    boolean checkIfUserExists(User user) throws DAOException;

    //Register user
    boolean registerUser(User user) throws DAOException;
}