package DAOs;

import DTOs.Movie;
import Exceptions.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLMovieDAO extends MySQLDAO implements MovieDAOInterface {

    @Override
    public int getRowCount() throws DAOException {
        String methodName = "getRowCount()";
        String query = "SELECT COUNT(*) AS count FROM movies";
        return getRowCount(methodName, query);
    }

    @Override
    public List<Movie> paginateMovies(int pageStart, int resultsPerPage) throws DAOException {
        String methodName = "paginateMovies()";
        String query = "SELECT * FROM movies ORDER BY title DESC LIMIT " + resultsPerPage + " OFFSET " + pageStart + ";";
        return getListOfMovies(methodName, query);
    }

    @Override
    public List<Movie> findMoviesByTitle(String title) throws DAOException {
        String methodName = "findMoviesByName()";
        String query = "SELECT * FROM movies WHERE title LIKE ?;";
        return getListOfMovies(methodName, query, title);
    }

    @Override
    public List<Movie> findMoviesByDirector(String director) throws DAOException {
        String methodName = "findMoviesByDirector()";
        String query = "SELECT * FROM MOVIES WHERE director LIKE ?;";
        return getListOfMovies(methodName, query, director);
    }

    @Override
    public List<Movie> findMoviesByGenre(String genre) throws DAOException {
        String methodName = "findMoviesByGenre()";
        String query = "SELECT * FROM MOVIES WHERE genre LIKE ?;";
        return getListOfMovies(methodName, query, genre);
    }

    @Override
    public List<Movie> findMoviesByActor(String actor) throws DAOException {
        String methodName = "findMoviesByActor()";
        String query = "SELECT * FROM MOVIES WHERE starring LIKE ?;";
        return getListOfMovies(methodName, query, actor);
    }

    /**
     * This method returns a list of recommended movies based on what the user has watched
     * @param userId
     * @return
     */
    @Override
    public List<String> recommendMovies(int userId) {
        ArrayList<String> recommendedMovieIDs = new ArrayList<String>();

        try {

            String mostPopularID = getMostPopularMovie();
            recommendedMovieIDs.add(mostPopularID);

            ArrayList<String> moviesWatched = getAllMoviesWatched(String.valueOf(userId));

            for (String watched : moviesWatched) {
                recommendedMovieIDs.addAll(getMoviesWatchedByOtherUsers(watched));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return recommendedMovieIDs;

    }

    /**
     * returns the id of the most watched movie in the database
     * @return
     * @throws SQLException
     */
    public String getMostPopularMovie() throws SQLException {
       /*
        Queries the database looking for the most watched movie to
        recommend to the user.
        */
        String query = "SELECT movie_id, COUNT(*) as magnitude FROM user_movies GROUP BY movie_id ORDER BY magnitude DESC LIMIT 1;";

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = getConnection();

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            String popularMovieID = String.valueOf(rs.getInt("movie_id"));

            return popularMovieID;
        } catch (SQLException e) {
            throw new DAOException("getMostPopularMovie " + e.getMessage());
        } finally {
            cleanEnvironment(rs, null, ps, con, null);
        }
    }

    /**
     * Returns a list of all movies watched by the user
     * @param userID
     * @return
     * @throws SQLException
     */

    public ArrayList<String> getAllMoviesWatched(String userID) throws SQLException {
        String query = "SELECT movie_id FROM user_movies WHERE user_id = ?";
        ArrayList<String> watchedMovies = new ArrayList<String>();

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, String.valueOf(userID));

            rs = ps.executeQuery();

            while (rs.next()) {
                watchedMovies.add(String.valueOf(rs.getInt("movie_id")));
            }
        } catch (SQLException e) {
            throw new DAOException("getAllMoviesWatched " + e.getMessage());
        } finally {
            cleanEnvironment(rs, null, ps, con, null);
        }

        return watchedMovies;
    }

    /**
     * Using the list of the user's watched movies reruns a list of what other users that have watched that movie have watched
     * @param movie_id
     * @return
     * @throws SQLException
     */
    public ArrayList<String> getMoviesWatchedByOtherUsers(String movie_id) throws SQLException {
        ArrayList<String> userIDs = new ArrayList<String>();
        ArrayList<String> movieIDs = new ArrayList<String>();
        //Get the list of all users that have watched the same movie
        String query = "SELECT user_id FROM user_movies where movie_id = ?";

        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = getConnection();

            ps = connection.prepareStatement(query);
            ps.setString(1, String.valueOf(movie_id));

            rs = ps.executeQuery();

            while (rs.next()) {
                userIDs.add(String.valueOf(rs.getInt("user_id")));
            }

            for (String user : userIDs) {
                query = "SELECT movie_id FROM user_movie WHERE user_id = " + user;
                ps = connection.prepareStatement(query);

                rs = ps.executeQuery();

                while (rs.next()) {
                    movieIDs.add(String.valueOf("movie_id"));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("getMoviesWatchedByOtherUSers " + e.getMessage());
        } finally {
            cleanEnvironment(rs, null, null, connection, null);
        }


        return movieIDs;
    }


    @Override
    public Movie watchMovie(int movieID) throws DAOException {
        if (checkIfMovieExists(movieID)) {
            String methodName = "playMovie()";
            String query = "SELECT * FROM movies WHERE id = " + movieID + " LIMIT 1";
            return getMovie(methodName, query);
        }
        return null;
    }

    @Override
    public boolean addMovie(Movie movie) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;

        String query = "INSERT INTO movies (title, genre, director, year) VALUES (?, ?, ?, ?);";

        try {
            con = getConnection();
            ps = con.prepareStatement(query);

            ps.setString(1, movie.getTitle());
            ps.setString(2, movie.getGenre());
            ps.setString(3, movie.getDirector());
            ps.setString(4, movie.getYear());

            return (ps.executeUpdate() > 0);
        } catch (SQLException e) {
            throw new DAOException("addMovie() " + e.getMessage());
        } finally {
            cleanEnvironment(null, null, ps, con, "addMovie()");
        }
    }

    @Override
    public boolean updateMovie(Movie movie) throws DAOException {
        if (checkIfMovieExists(movie)) {
            Connection con = null;
            PreparedStatement ps = null;

            String query = "UPDATE movies SET title = ?, rating = ?, user_rating = ?, format = ? WHERE id = " + movie.getId() + ";";

            try {
                con = getConnection();
                ps = con.prepareStatement(query);

                ps.setString(1, movie.getTitle());
                ps.setString(2, movie.getRating());
                ps.setString(3, movie.getUserRating());
                ps.setString(4, movie.getFormat());

                return (ps.executeUpdate() > 0);
            } catch (SQLException e) {
                throw new DAOException("updateMovie() " + e.getMessage());
            } finally {
                cleanEnvironment(null, null, ps, con, "updateMovie()");
            }
        }
        return false;
    }

    @Override
    public boolean removeMovie(int movieID) throws DAOException {
        String methodName = "removeMovie()";
        String query = "DELETE FROM movies WHERE id = " + movieID + " LIMIT 1";
        return removeFromTable(methodName, query);
    }

    @Override
    public boolean checkIfMovieExists(Movie movie) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT COUNT(*) AS count FROM movies WHERE title LIKE ? LIMIT 1";

        try {
            con = getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, movie.getTitle());
            rs = ps.executeQuery();

            rs.next();
            return (rs.getInt("count") > 0);
        } catch (SQLException e) {
            throw new DAOException("checkIfMovieExists() " + e.getMessage());
        } finally {
            this.cleanEnvironment(rs, null, ps, con, "checkIfMovieExists()");
        }
    }

    @Override
    public boolean checkIfMovieExists(int id) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT COUNT(*) AS count FROM movies WHERE id = ? LIMIT 1";

        System.out.println(id);

        try {
            con = getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            rs.next();
            return (rs.getInt("count") > 0);
        } catch (SQLException e) {
            throw new DAOException("checkIfMovieExists() " + e.getMessage());
        } finally {
            this.cleanEnvironment(rs, null, ps, con, "checkIfMovieExists()");
        }
    }

    private Movie getMovie(String methodName, String query) throws DAOException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        Movie movie = null;

        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            if (rs.next()) movie = createMovieObject(rs);

        } catch (SQLException e) {
            throw new DAOException(methodName + " " + e.getMessage());
        } finally {
            cleanEnvironment(rs, stmt, null, con, methodName);
        }
        return movie;
    }

    @Override
    public Movie getMovieById(int movieId) throws DAOException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        Movie movie = null;
        String query = "SELECT * FROM movies WHERE id = " + movieId + ";";

        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            if (rs.next()) movie = createMovieObject(rs);

        } catch (SQLException e) {
            throw new DAOException("getMovieById " + e.getMessage());
        } finally {
            cleanEnvironment(rs, stmt, null, con, "getMovieById");
        }
        return movie;
    }

    private List<Movie> getListOfMovies(String methodName, String query) throws DAOException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        Movie movie;
        List<Movie> movies = new ArrayList<>();

        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                movie = createMovieObject(rs);
                if (movie != null) {
                    movies.add(movie);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(methodName + " " + e.getMessage());
        } finally {
            cleanEnvironment(rs, stmt, null, con, methodName);
        }
        return movies;
    }

    private List<Movie> getListOfMovies(String methodName, String query, String parameter) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        Movie movie;
        List<Movie> movies = new ArrayList<>();

        try {
            con = getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, "%" + parameter + "%");
            rs = ps.executeQuery();

            while (rs.next()) {
                movie = createMovieObject(rs);
                if (movie != null) {
                    movies.add(movie);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(methodName + " " + e.getMessage());
        } finally {
            cleanEnvironment(rs, null, ps, con, methodName);
        }
        return movies;
    }

    private Movie createMovieObject(ResultSet rs) {
        try {
            int id = rs.getInt("id");
            String title = rs.getString("title");
            String genre = rs.getString("genre");
            String director = rs.getString("director");
            String runtime = rs.getString("runtime");
            String plot = rs.getString("plot");
            String location = rs.getString("location");
            String poster = rs.getString("poster");
            String rating = rs.getString("rating");
            String format = rs.getString("format");
            String year = rs.getString("year");
            String starring = rs.getString("starring");
            String copies = rs.getString("copies");
            String barcode = rs.getString("barcode");
            String user_rating = rs.getString("user_rating");
            return new Movie(id, title, genre, director, runtime, plot, location, poster, rating, format, year, starring, copies, barcode, user_rating);
        } catch (SQLException se) {
            se.printStackTrace();
        }
        return null;
    }

    public void addWatched(int movieID, int userID) throws DAOException {

        Connection conn = null;
        PreparedStatement ps = null;

        String query = "INSERT INTO user_movies VALUES(?,?)";

        try {
            conn = getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, movieID);
            ps.setInt(2, userID);

            ps.executeUpdate();
        } catch (SQLException se) {
            throw new DAOException("addedWatched: " + se.getMessage());
        } finally {
            cleanEnvironment(null, null, ps, conn, null);
        }
    }
}