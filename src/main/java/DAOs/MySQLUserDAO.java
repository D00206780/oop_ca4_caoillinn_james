package DAOs;

import DTOs.User;
import Exceptions.DAOException;

import java.sql.*;
import java.util.ArrayList;

public class MySQLUserDAO extends MySQLDAO implements UserDAOInterface {

    @Override
    public User findUserByUsernamePassword(String username, String password) throws DAOException {
        String[] parameters = {username, password};
        String methodName = "findUserByUsernamePassword()";
        String query = "SELECT * FROM user WHERE username = ? AND password = ?;";
        return getUser(methodName, query, parameters);
    }

    @Override
    public boolean registerUser(User user) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;

        ArrayList<String> userDetails = user.toArrayList();
        String query = "INSERT INTO user (first_name, last_name, username, password) VALUES (?, ?, ?, ?);";

        try {
            con = getConnection();
            ps = con.prepareStatement(query);

            for (int i = 0; i < userDetails.size(); i++) {
                ps.setString(i + 1, userDetails.get(i));
            }

            return (ps.executeUpdate() > 0);
        } catch (SQLException e) {
            throw new DAOException("registerUser() " + e.getMessage());
        } finally {
            cleanEnvironment(null, null, ps, con, "registerUser()");
        }
    }

    public int getLastInsertId() throws DAOException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        int lastId = -1;
        String query = "SELECT MAX(id) FROM user;";

        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                lastId = rs.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("getLastInsertId() " + e.getMessage());
        } finally {
            cleanEnvironment(rs, stmt, null, con, "getLastInsertId");
        }
        return lastId;
    }

    @Override
    public boolean checkIfUserExists(User user) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT COUNT(*) AS count FROM user WHERE username LIKE ? LIMIT 1;";

        try {
            con = getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, user.getUsername());
            rs = ps.executeQuery();

            rs.next();
            return (rs.getInt("count") > 0);
        } catch (SQLException e) {
            throw new DAOException("checkIfUserExists() " + e.getMessage());
        } finally {
            this.cleanEnvironment(rs, null, ps, con, "checkIfUserExists()");
        }
    }

    private User getUser(String methodName, String query, String[] parameters) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        User user = null;

        try {
            con = getConnection();
            ps = con.prepareStatement(query);

            for (int i = 0; i < parameters.length; i++) {
                ps.setString(i + 1, parameters[i]);
            }

            rs = ps.executeQuery();
            if (rs.next()) user = createUserObject(rs);

        } catch (SQLException e) {
            throw new DAOException(methodName + " " + e.getMessage());
        } finally {
            cleanEnvironment(rs, null, ps, con, methodName);
        }
        return user;
    }

    private User createUserObject(ResultSet rs) {
        try {
            int userId = rs.getInt("id");
            String firstname = rs.getString("first_name");
            String lastname = rs.getString("last_name");
            String username = rs.getString("username");
            String password = rs.getString("password");
            return new User(userId, firstname, lastname, username, password);
        } catch (SQLException se) {
            se.printStackTrace();
        }
        return null;
    }
}