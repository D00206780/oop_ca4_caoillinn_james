package DAOs;

import Exceptions.DAOException;

import java.sql.*;

public class MySQLDAO implements DAOInterface {

    boolean removeFromTable(String methodName, String query) throws DAOException {
        Connection con = null;
        Statement stmt = null;

        try {
            con = getConnection();
            stmt = con.createStatement();
            if (stmt.executeUpdate(query) >= 1) return true;
        } catch (SQLException SQLe) {
            throw new DAOException(methodName + " " + SQLe.getMessage());
        } finally {
            cleanEnvironment(null, stmt, null, con, methodName);
        }
        return false;
    }

    @Override
    public int getRowCount(String methodName, String query) throws DAOException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        int count = 0;

        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                count = rs.getInt("count");
            }
        } catch (SQLException SQLe) {
            throw new DAOException(methodName + " " + SQLe.getMessage());
        } finally {
            this.cleanEnvironment(rs, stmt, null, con, methodName);
        }
        return count;
    }

    Connection getConnection() {

        String url = "jdbc:mysql://localhost:3306/gd2oopca4?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT";
        String username = "root";
        String password = "";
        Connection con = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Connection failed: " + ex.getMessage());
            System.exit(1);
        }
        return con;
    }

    void cleanEnvironment(ResultSet rs, Statement s, PreparedStatement ps, Connection con, String method) throws DAOException {
        try {
            if (rs != null) {
                rs.close();
            }
            if (s != null) {
                s.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                closeConnection(con);
            }
        } catch (SQLException SQLe) {
            throw new DAOException(method + " " + SQLe.getMessage());
        }
    }

    private void closeConnection(Connection con) {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException SQLe) {
            System.out.println("Failed to free the connection: " + SQLe.getMessage());
            System.exit(1);
        }
    }
}