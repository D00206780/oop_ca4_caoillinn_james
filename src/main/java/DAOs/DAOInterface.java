package DAOs;

import Exceptions.DAOException;

public interface DAOInterface {

    //Return a count of the rows in a table
    int getRowCount(String methodName, String query) throws DAOException;

}